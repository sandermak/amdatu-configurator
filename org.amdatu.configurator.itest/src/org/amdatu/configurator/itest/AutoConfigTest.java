/*
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.configurator.itest;

import java.util.Dictionary;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import junit.framework.TestCase;

import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.cm.ManagedServiceFactory;

/**
 * Test cases for Amdatu AutoConf.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class AutoConfigTest extends TestCase {
    private final BundleContext m_context = FrameworkUtil.getBundle(this.getClass()).getBundleContext();

    /**
     * Tests that the configurations are indeed provisioned.
     */
    public void testConfigurationsProvisionedOk() throws Exception {
        ConfigurationAdmin configAdmin = getConfigAdmin();

        Configuration[] configs = configAdmin.listConfigurations(null);
        assertNotNull("No configurations found?!", configs);
        assertEquals(10, configs.length);

        Configuration config = configAdmin.getConfiguration("org.amdatu.conf3", null);
        assertNotNull(config);

        assertEquals(m_context.getProperty("context.serverUrl"), config.getProperties().get("serverUrl"));
    }

    /**
     * Tests that a provisioned configuration is actually delivered to the correct managed service.
     */
    public void testObtainAutoConfConfigurationAsManagedServiceOk() throws Exception {
        final CountDownLatch latch = new CountDownLatch(1);

        ManagedService service = new ManagedService() {
            @Override
            public void updated(Dictionary properties) throws ConfigurationException {
                if (properties != null && "http://conf1:8080/".equals(properties.get("server"))) {
                    latch.countDown();
                }
            }
        };

        Properties props = new Properties();
        props.put(Constants.SERVICE_PID, "org.amdatu.conf1");

        ServiceRegistration reg = m_context.registerService(ManagedService.class.getName(), service, props);

        assertTrue("No configuration was bound to our service in time?!", latch.await(5, TimeUnit.SECONDS));

        reg.unregister();
    }

    /**
     * Tests that a provisioned configuration is actually delivered to the correct managed service.
     */
    public void testObtainPropertiesConfigurationAsManagedServiceOk() throws Exception {
        final CountDownLatch latch = new CountDownLatch(1);

        ManagedService service = new ManagedService() {
            @Override
            public void updated(Dictionary properties) throws ConfigurationException {
                if (properties != null && "http://localhost:9000/".equals(properties.get("server"))) {
                    latch.countDown();
                }
            }
        };

        Properties props = new Properties();
        props.put(Constants.SERVICE_PID, "org.amdatu.conf5");

        ServiceRegistration reg = m_context.registerService(ManagedService.class.getName(), service, props);

        assertTrue("No configuration was bound to our service in time?!", latch.await(5, TimeUnit.SECONDS));

        reg.unregister();
    }

    /**
     * Tests that a provisioned configuration is actually delivered to the correct managed service.
     */
    public void testObtainAutoConfConfigurationAsManagedServiceFactoryOk() throws Exception {
        final CountDownLatch latch = new CountDownLatch(2);

        ManagedServiceFactory service = new ManagedServiceFactory() {
            @Override
            public void updated(String pid, Dictionary properties) throws ConfigurationException {
                Object gear = properties.get("gear");
                if (Integer.valueOf(2).equals(gear) || Integer.valueOf(3).equals(gear)) {
                    latch.countDown();
                }
            }

            @Override
            public String getName() {
                return "My service factory";
            }

            @Override
            public void deleted(String pid) {
                fail("Should not occur!");
            }
        };

        Properties props = new Properties();
        props.put(Constants.SERVICE_PID, "org.amdatu.conf2.factory");

        ServiceRegistration reg = m_context.registerService(ManagedServiceFactory.class.getName(), service, props);

        assertTrue("No configuration was bound to our service in time?!", latch.await(5, TimeUnit.SECONDS));

        reg.unregister();
    }

    /**
     * Tests that a provisioned configuration is actually delivered to the correct managed service.
     */
    public void testObtainPropertiesConfigurationAsManagedServiceFactoryOk() throws Exception {
        final CountDownLatch latch = new CountDownLatch(2);

        ManagedServiceFactory service = new ManagedServiceFactory() {
            @Override
            public void updated(String pid, Dictionary properties) throws ConfigurationException {
                Object gear = properties.get("gear");
                if ("2".equals(gear) || "3".equals(gear)) {
                    latch.countDown();
                }
            }

            @Override
            public String getName() {
                return "My service factory";
            }

            @Override
            public void deleted(String pid) {
                fail("Should not occur!");
            }
        };

        Properties props = new Properties();
        props.put(Constants.SERVICE_PID, "org.amdatu.conf6.factory");

        ServiceRegistration reg = m_context.registerService(ManagedServiceFactory.class.getName(), service, props);

        assertTrue("No configuration was bound to our service in time?!", latch.await(5, TimeUnit.SECONDS));

        reg.unregister();
    }

    @Override
    protected void setUp() throws Exception {
        assertNotNull("No bundle context, is this test running as OSGi test?", m_context);
    }

    private ConfigurationAdmin getConfigAdmin() {
        ServiceReference serviceRef = m_context.getServiceReference(ConfigurationAdmin.class.getName());
        assertNotNull("No ConfigAdmin found?!", serviceRef);

        return (ConfigurationAdmin) m_context.getService(serviceRef);
    }
}
