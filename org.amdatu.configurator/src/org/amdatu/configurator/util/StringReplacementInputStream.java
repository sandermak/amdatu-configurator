/*
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.configurator.util;

import java.io.ByteArrayOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;

/**
 * Provides an input stream that is capable of replacing parts of the content while it is being streamed.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class StringReplacementInputStream extends FilterInputStream {
    private static final int BUFFER_SIZE = 100;

    private static final byte DOLLAR = '$';
    private static final byte OPEN_BR = '{';
    private static final byte CLOSE_BR = '}';

    private final Replacer m_replacer;

    /**
     * Creates a new {@link StringReplacementInputStream} instance.
     */
    public StringReplacementInputStream(InputStream in, Replacer replacer) {
        super(new PushbackInputStream(in, BUFFER_SIZE));

        m_replacer = replacer;
    }

    @Override
    public int read() throws IOException {
        int read = super.read();
        if (read == DOLLAR) {
            int la = super.read();
            if (la == OPEN_BR) {
                // consume until we see a '}', or EOF...
                ByteArrayOutputStream baos = new ByteArrayOutputStream(BUFFER_SIZE);
                int ch = 0, no = 0, max = BUFFER_SIZE - 3;
                do {
                    ch = super.read();
                    if (ch < 0 || ch == CLOSE_BR) {
                        break;
                    }
                    no++;
                    baos.write(ch);
                }
                while (no < max);

                String name = baos.toString();
                if (ch >= 0) {
                    String replacement = m_replacer.replace(name);
                    if (replacement != null) {
                        byte[] bytes = replacement.getBytes();
                        pushback(bytes, 1, bytes.length - 1);
                        return bytes[0];
                    }
                }

                if (ch == CLOSE_BR) {
                    pushback(CLOSE_BR);
                }
                pushback(name.getBytes());
                pushback(OPEN_BR);
            }
            else {
                pushback((byte) la, DOLLAR);
                return DOLLAR;
            }
        }
        return read;
    }

    @Override
    public int read(byte[] b) throws IOException {
        return read(b, 0, b.length);
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        if (b == null) {
            throw new NullPointerException();
        }
        else if (off < 0 || len < 0 || len > b.length - off) {
            throw new IndexOutOfBoundsException();
        }
        else if (len == 0) {
            return 0;
        }

        int c = read();
        if (c == -1) {
            return -1;
        }
        b[off] = (byte) c;

        int i = 1;
        for (; i < len; i++) {
            c = read();
            if (c == -1) {
                break;
            }
            b[off + i] = (byte) c;
        }
        return i;
    }

    private void pushback(byte... chs) throws IOException {
        ((PushbackInputStream) in).unread(chs);
    }

    private void pushback(byte[] chs, int off, int len) throws IOException {
        ((PushbackInputStream) in).unread(chs, off, len);
    }

    /**
     * Represents a simple property replacer, which can replace a given placeholder name for another value.
     * 
     * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
     */
    public static interface Replacer {
        /**
         * Returns the value for the placeholder with the given name.
         * 
         * @param input the placeholder name to replace, can be <code>null</code>.
         * @return the replacement value, or <code>null</code> if no replacement is available.
         */
        String replace(String input);
    }
}
