/*
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.configurator.autoconf;

import static org.amdatu.configurator.autoconf.MetaTypeUtil.listAutoConfResources;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.amdatu.configurator.util.Logger;
import org.apache.felix.metatype.MetaData;
import org.apache.felix.metatype.OCD;

/**
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class AutoConfResources implements Iterable<AutoConfResource> {
    private final List<AutoConfResource> m_resources;
    private final MetaData m_metaData;
    private final Logger m_logger;

    /**
     * Creates a new {@link AutoConfResources} instance.
     */
    public AutoConfResources(MetaData metaData, Logger logger) {
        m_metaData = metaData;
        m_resources = listAutoConfResources(logger, metaData);
        m_logger = logger;
    }

    @Override
    public Iterator<AutoConfResource> iterator() {
        return m_resources.iterator();
    }

    /**
     * @return <code>true</code> if the {@link AutoConfResource}s are valid and correct, <code>false</code> otherwise.
     */
    public boolean verify() {
        Map<String, OCD> localOCDs = m_metaData.getObjectClassDefinitions();
        if (localOCDs == null) {
            m_logger.warn("No OCDs defined! Verification disabled...");
            return true;
        }

        for (AutoConfResource resource : m_resources) {
            if (!resource.verify(localOCDs)) {
                return false;
            }
        }

        return true;
    }
}
