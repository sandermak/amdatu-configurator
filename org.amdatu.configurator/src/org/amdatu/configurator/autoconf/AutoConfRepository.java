/*
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.configurator.autoconf;

import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.StreamCorruptedException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.util.HashMap;
import java.util.Map;

import org.apache.felix.metatype.Designate;
import org.osgi.service.cm.Configuration;

/**
 * Provides a persistent repository for mapping provisioned configurations to their aliases.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class AutoConfRepository {
    private final Map<String, String> m_mapping;

    /**
     * Creates a new {@link AutoConfRepository} instance.
     */
    public AutoConfRepository() {
        m_mapping = new HashMap<>();
    }

    /**
     * Adds a mapping for a provisioned {@link Configuration} to an {@link AutoConfResource}.
     * 
     * @param configuration the provisioned {@link Configuration} to map, cannot be <code>null</code>;
     * @param resource the {@link AutoConfResource} that maps to the given configuration, cannot be <code>null</code>.
     * @return <code>true</code> if the mapping was added (did not exist yet), <code>false</code> otherwise.
     */
    public boolean add(Configuration configuration, AutoConfResource resource) {
        String id = createIdentity(resource);
        String pid = configuration.getPid();
        if (!m_mapping.containsKey(pid)) {
            m_mapping.put(pid, id);
            return true;
        }
        return false;
    }

    /**
     * Returns the PID of the configuration provisioned for the given {@link AutoConfResource}.
     * 
     * @param resource the {@link AutoConfResource} to return the configuration PID for, cannot be <code>null</code>.
     * @return the configuration PID as string, or <code>null</code> if no configuration was mapped to the given
     *         {@link AutoConfResource}.
     */
    public String getConfigurationPid(AutoConfResource resource) {
        String id = createIdentity(resource);
        for (Map.Entry<String, String> entry : m_mapping.entrySet()) {
            if (id.equals(entry.getValue())) {
                return entry.getKey();
            }
        }
        return null;
    }

    /**
     * @return <code>true</code> if this repository is empty, <code>false</code> otherwise.
     */
    public boolean isEmpty() {
        return m_mapping.isEmpty();
    }

    /**
     * Loads the contents of this repository from a given {@link File}.
     * 
     * @param repoFile the repository file to load the data from, cannot be <code>null</code>.
     * @throws IOException in case of I/O problems reading from the repository file.
     */
    public void load(File repoFile) throws IOException {
        try (InputStream fis = Files.newInputStream(repoFile.toPath()); ObjectInputStream ois = new ObjectInputStream(fis)) {
            m_mapping.putAll((Map<String, String>) ois.readObject());
        }
        catch (NoSuchFileException | EOFException exception) {
            // Ignore, first time start...
        }
        catch (ClassNotFoundException | StreamCorruptedException exception) {
            throw new IOException("Invalid repository data?!", exception);
        }
    }

    /**
     * Stores the contents of this repository to a given {@link File}.
     * 
     * @param repoFile the repository file to store the data to, cannot be <code>null</code>.
     * @throws IOException in acse of I/O problems writing to the repository file.
     */
    public void store(File repoFile) throws IOException {
        try (OutputStream fos = Files.newOutputStream(repoFile.toPath()); ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            oos.writeObject(m_mapping);
        }
    }

    private String createIdentity(AutoConfResource resource) {
        Designate designate = resource.getDesignate();
        String factoryPid = designate.getFactoryPid();
        if (factoryPid != null) {
            return String.format("%s-%s", factoryPid, designate.getPid());
        }
        return designate.getPid();
    }
}
