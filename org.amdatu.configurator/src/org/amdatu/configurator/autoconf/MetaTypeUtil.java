/*
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.configurator.autoconf;

import static org.osgi.service.metatype.AttributeDefinition.BOOLEAN;
import static org.osgi.service.metatype.AttributeDefinition.BYTE;
import static org.osgi.service.metatype.AttributeDefinition.CHARACTER;
import static org.osgi.service.metatype.AttributeDefinition.DOUBLE;
import static org.osgi.service.metatype.AttributeDefinition.FLOAT;
import static org.osgi.service.metatype.AttributeDefinition.INTEGER;
import static org.osgi.service.metatype.AttributeDefinition.LONG;
import static org.osgi.service.metatype.AttributeDefinition.SHORT;
import static org.osgi.service.metatype.AttributeDefinition.STRING;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.amdatu.configurator.util.Logger;
import org.apache.felix.metatype.Attribute;
import org.apache.felix.metatype.Designate;
import org.apache.felix.metatype.DesignateObject;
import org.apache.felix.metatype.MetaData;
import org.apache.felix.metatype.OCD;

/**
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class MetaTypeUtil {
    /**
     * Creates a new {@link MetaTypeUtil} instance.
     */
    private MetaTypeUtil() {
        // Nop
    }

    /**
     * Extracts all designates from the given {@link MetaData} object and converts it to an {@link AutoConfResource}.
     * 
     * @param logger the logger to report problems to;
     * @param data the {@link MetaData} to convert.
     * @return a list of {@link AutoConfResource}s to provision, never <code>null</code>.
     */
    public static List<AutoConfResource> listAutoConfResources(Logger logger, MetaData data) {
        Map<String, Designate> designates = data.getDesignates();
        Map<String, OCD> localOcds = data.getObjectClassDefinitions();

        List<AutoConfResource> result = new ArrayList<>();
        if (designates != null) {
            for (Designate designate : designates.values()) {
                DesignateObject object = designate.getObject();
                if (object != null) {
                    String ocdRef = object.getOcdRef();
                    OCD ocd = localOcds.get(ocdRef);

                    result.add(new AutoConfResource(logger, designate, ocd));
                }
                else {
                    logger.warn("Ignoring empty designate with pid=\"%s\", factoryPid=\"%s\"...", designate.getPid(), designate.getFactoryPid());
                }
            }
        }
        else {
            logger.warn("No designates defined, plain MetaType file given? Source = \"%s\".", data.getSource());
        }
        return result;
    }

    /**
     * Determines the value of an attribute based on an attribute definition
     * 
     * @param attribute The attribute containing value(s)
     * @param ad The attribute definition
     * @return An <code>Object</code> reflecting what was specified in the attribute and it's definition or
     *         <code>null</code> if
     *         the value did not match it's definition.
     */
    public static Object getValue(Logger logger, Attribute attribute, int type, int cardinality) {
        String[] content = attribute.getContent();

        // verify correct type of the value(s)
        Object[] typedContent = null;
        try {
            for (int i = 0; i < content.length; i++) {
                String value = content[i];
                switch (type) {
                    case BOOLEAN:
                        typedContent = (typedContent == null) ? new Boolean[content.length] : typedContent;
                        typedContent[i] = Boolean.valueOf(value);
                        break;
                    case BYTE:
                        typedContent = (typedContent == null) ? new Byte[content.length] : typedContent;
                        typedContent[i] = Byte.valueOf(value);
                        break;
                    case CHARACTER:
                        typedContent = (typedContent == null) ? new Character[content.length] : typedContent;
                        char[] charArray = value.toCharArray();
                        if (charArray.length == 1) {
                            typedContent[i] = new Character(charArray[0]);
                        }
                        else {
                            return null;
                        }
                        break;
                    case DOUBLE:
                        typedContent = (typedContent == null) ? new Double[content.length] : typedContent;
                        typedContent[i] = Double.valueOf(value);
                        break;
                    case FLOAT:
                        typedContent = (typedContent == null) ? new Float[content.length] : typedContent;
                        typedContent[i] = Float.valueOf(value);
                        break;
                    case INTEGER:
                        typedContent = (typedContent == null) ? new Integer[content.length] : typedContent;
                        typedContent[i] = Integer.valueOf(value);
                        break;
                    case LONG:
                        typedContent = (typedContent == null) ? new Long[content.length] : typedContent;
                        typedContent[i] = Long.valueOf(value);
                        break;
                    case SHORT:
                        typedContent = (typedContent == null) ? new Short[content.length] : typedContent;
                        typedContent[i] = Short.valueOf(value);
                        break;
                    case STRING:
                        typedContent = (typedContent == null) ? new String[content.length] : typedContent;
                        typedContent[i] = value;
                        break;
                    default:
                        // unsupported type
                        return null;
                }
            }
        }
        catch (NumberFormatException nfe) {
            return null;
        }

        // verify cardinality of value(s)
        Object result = null;
        if (cardinality == 0) {
            if (typedContent.length == 1) {
                result = typedContent[0];
            }
            else {
                result = null;
            }
        }
        else if (cardinality == Integer.MIN_VALUE) {
            result = new Vector(Arrays.asList(typedContent));
        }
        else if (cardinality == Integer.MAX_VALUE) {
            result = typedContent;
        }
        else if (cardinality < 0) {
            if (typedContent.length <= Math.abs(cardinality)) {
                result = new Vector(Arrays.asList(typedContent));
            }
            else {
                result = null;
            }
        }
        else if (cardinality > 0) {
            if (typedContent.length <= cardinality) {
                result = typedContent;
            }
            else {
                result = null;
            }
        }
        return result;
    }
}
