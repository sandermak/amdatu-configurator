/*
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.configurator.autoconf;

import static org.amdatu.configurator.autoconf.MetaTypeUtil.getValue;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.amdatu.configurator.util.Logger;
import org.apache.felix.metatype.AD;
import org.apache.felix.metatype.Attribute;
import org.apache.felix.metatype.Designate;
import org.apache.felix.metatype.DesignateObject;
import org.apache.felix.metatype.OCD;

/**
 * Represents a single configuration in AutoConf format.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class AutoConfResource {
    private static final String LOCATION_PREFIX = "osgi-dp:";

    private final Logger m_logger;
    private final Designate m_designate;
    private final OCD m_ocd;

    /**
     * Creates a new {@link AutoConfResource} instance.
     */
    public AutoConfResource(Logger logger, Designate designate, OCD ocd) {
        if (designate == null) {
            throw new IllegalArgumentException("Invalid designate!");
        }

        DesignateObject object = designate.getObject();
        if (ocd == null || !ocd.getID().equals(object.getOcdRef())) {
            throw new IllegalArgumentException("Invalid OCD!");
        }

        m_logger = logger;
        m_designate = designate;
        m_ocd = ocd;
    }

    private static boolean isEmtpy(String str) {
        return str == null || "".equals(str.trim());
    }

    /**
     * Returns the bundle location the AutoConf resource should be provisioned to.
     * <p>
     * Locations starting with the prefix "osgi-dp:" are stripped from this prefix, allowing AutoConf resources used in
     * Deployment Admin to be used as well.
     * </p>
     * 
     * @return the bundle location, never <code>null</code>.
     */
    public String getBundleLocation() {
        String loc = m_designate.getBundleLocation();
        if ((loc != null) && loc.startsWith(LOCATION_PREFIX)) {
            loc = loc.substring(LOCATION_PREFIX.length());
        }
        return loc;
    }

    /**
     * @return the actual configuration properties, never <code>null</code>.
     */
    public Dictionary<String, ?> getConfiguration() {
        DesignateObject object = m_designate.getObject();

        List<Attribute> attributes = object.getAttributes();
        Map<String, AD> attributeDefs = m_ocd.getAttributeDefinitions();

        Dictionary<String, Object> props = new Hashtable<>(attributes.size());
        for (Attribute attribute : attributes) {
            String adRef = attribute.getAdRef();

            AD attributeDef = attributeDefs.get(adRef);
            if (attributeDef != null) {
                props.put(adRef, getValue(m_logger, attribute, attributeDef.getType(), attributeDef.getCardinality()));
            }
        }

        return props;
    }

    /**
     * @return the designate for this resource, never <code>null</code>.
     */
    public Designate getDesignate() {
        return m_designate;
    }

    /**
     * @return a "unique" identifier for this AutoConf resource, never <code>null</code>.
     */
    public String getId() {
        String id = m_designate.getFactoryPid();
        if (isEmtpy(id)) {
            id = m_designate.getPid();
        }
        return id;
    }

    /**
     * @return <code>true</code> if this AutoConf resource represents a factory configuration, <code>false</code> if it
     *         represents a "singleton" configuration.
     */
    public boolean isFactoryConfig() {
        String factoryPid = m_designate.getFactoryPid();
        return !isEmtpy(factoryPid);
    }

    /**
     * @param logger
     * @param localOcds
     * @return
     */
    public boolean verify(Map<String, OCD> localOcds) {
        DesignateObject object = m_designate.getObject();
        if (object == null) {
            m_logger.warn("Designate Object child missing or invalid for Designate \"%s\"", m_designate.getPid());
            return false;
        }

        String ocdRef = object.getOcdRef();
        if (ocdRef == null || "".equals(ocdRef)) {
            m_logger.warn("Object ocdRef attribute missing or invalid for Designate \"%s\"", m_designate.getPid());
            return false;
        }

        List<Attribute> attributeList = object.getAttributes();
        if (attributeList == null || attributeList.isEmpty()) {
            m_logger.warn("Object Attributes child missing or invalid for Designate \"%s\"", m_designate.getPid());
            return false;
        }

        Map<String, Attribute> attributes = new HashMap<>();
        for (Attribute attribute : attributeList) {
            if (attributes.put(attribute.getAdRef(), attribute) != null) {
                m_logger.warn("Duplicate Object Attribute \"%s\" in Designate \"%s\"", attribute.getAdRef(), m_designate.getPid());
                return false;
            }
        }

        OCD localOcd = localOcds.get(ocdRef);
        if (localOcd == null) {
            m_logger.warn("No OCD found with id \"%s\" for Designate \"%s\"", ocdRef, m_designate.getPid());
            return false;
        }

        Map<String, AD> attributeDefs = localOcd.getAttributeDefinitions();
        // Validate attributes...
        for (Map.Entry<String, AD> entry : attributeDefs.entrySet()) {
            if (!verifyAttribute(entry.getValue(), attributes.get(entry.getKey()))) {
                return false;
            }
        }

        return true;
    }

    private boolean verifyAttribute(AD attributeDef, Attribute attribute) {
        if (attribute == null) {
            // wrong attribute or definition
            m_logger.warn("No attribute found for AD \"%s\" in Designate \"%s\"", attributeDef.getID(), m_designate.getPid());
            return false;
        }

        Object value = getValue(m_logger, attribute, attributeDef.getType(), attributeDef.getCardinality());
        if (value == null && (!m_designate.isOptional() || attributeDef.isRequired())) {
            m_logger.warn("No value for attribute \"%s\" in Designate \"%s\"", attributeDef.getID(), m_designate.getPid());
            return false;
        }

        return true;
    }
}
