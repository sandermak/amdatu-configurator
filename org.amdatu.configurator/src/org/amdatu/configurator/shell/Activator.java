/*
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.configurator.shell;

import static org.amdatu.configurator.shell.ConfigCommands.FUNCTIONS;
import static org.amdatu.configurator.shell.ConfigCommands.SCOPE;
import static org.apache.felix.service.command.CommandProcessor.COMMAND_FUNCTION;
import static org.apache.felix.service.command.CommandProcessor.COMMAND_SCOPE;

import java.util.Properties;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.log.LogService;

/**
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class Activator extends DependencyActivatorBase {

    @Override
    public void destroy(BundleContext context, DependencyManager manager) throws Exception {
        // Nop
    }

    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {
        manager.add(createComponent() //
            .setInterface(Object.class.getName(), createProps(SCOPE, FUNCTIONS)) //
            .setImplementation(ConfigCommands.class) //
            .add(createServiceDependency().setService(ConfigurationAdmin.class).setRequired(true)) //
            .add(createServiceDependency().setService(LogService.class).setRequired(false)) //
            );
    }

    private Properties createProps(String scope, String[] functions) {
        Properties props = new Properties();
        props.put(COMMAND_SCOPE, scope);
        props.put(COMMAND_FUNCTION, functions);
        return props;
    }
}
