/*
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.configurator.properties;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Dictionary;
import java.util.Properties;

import org.amdatu.configurator.util.Logger;
import org.amdatu.configurator.util.StringReplacementInputStream;
import org.amdatu.configurator.util.StringReplacementInputStream.Replacer;
import org.apache.felix.dm.Component;
import org.osgi.framework.BundleContext;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.log.LogService;

/**
 * Reads Java properties files and provisions them to ConfigurationAdmin.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class PropertyConfigurator {
    public static final String KEY_CONFIG_DIR = "org.amdatu.configurator.properties.dir";
    public static final String KEY_VERBOSE = "org.amdatu.configurator.properties.verbose";
    public static final String KEY_PLACEHOLDER_PREFIX = "org.amdatu.configurator.properties.prefix";

    /** Default directory to use in case no directory is specified. */
    public static final String DEFAULT_CONFIG_DIR = "conf";
    /** Prefix used for placeholders in ACE. */
    public static final String DEFAULT_PLACEHOLDER_PREFIX = "context.";

    // Injected by Felix DM...
    private volatile BundleContext m_context;
    private volatile ConfigurationAdmin m_configAdmin;
    private volatile LogService m_logService;

    private final PropertyFileRepository m_repository;

    /**
     * Creates a new {@link PropertyConfigurator} instance.
     */
    public PropertyConfigurator() {
        m_repository = new PropertyFileRepository();
    }

    /**
     * Installs all Property-file resources found in the configuration directory.
     * <p>
     * This method tries to install <em>all</em> found Property-file resources, continuing with the next Property-file
     * resource upon exceptions. Only the <em>first</em> exception is rethrown after all Property-file resources have
     * been processed.</p>
     * 
     * @throws IOException in case of I/O errors accessing Property-file resources.
     */
    public void install() throws IOException {
        Exception firstException = null;

        File[] files = listConfigurations();
        for (File file : files) {
            try {
                install(file);
            }
            catch (Exception exception) {
                if (firstException == null) {
                    firstException = exception;
                }
                warn("Failed to provision Property-file resource from \"%s\"...", exception, file.getName());
            }
        }

        if (firstException instanceof RuntimeException) {
            throw (RuntimeException) firstException;
        }
        else if (firstException instanceof IOException) {
            throw (IOException) firstException;
        }
        else if (firstException != null) {
            throw new RuntimeException("Failed to provision Property-file resource!", firstException);
        }
    }

    /**
     * Regards the given {@link File} as Property-file resource and installs it.
     * 
     * @param file the {@link File} to install as Property-file resource, cannot be <code>null</code>.
     * @throws IOException in case of I/O errors accessing the Property-file resource.
     */
    public void install(File file) throws IOException {
        if (isVerbose()) {
            info("Installing configuration from \"%s\"...", file.getName());
        }

        try (InputStream fis = Files.newInputStream(file.toPath()); InputStream is = new StringReplacementInputStream(fis, new FrameworkPropertyReplacer())) {
            // Read the entry as MetaType file...
            Properties props = new Properties();
            props.load(is);

            PropertyFileResource resource = new PropertyFileResource(file, props);
            if (isVerbose()) {
                info("Processing Property-file resource \"%s\"...", resource.getId());
            }
            provision(resource);
        }
    }

    void debug(String msg, Object... args) {
        String logMsg = String.format(msg, args);
        m_logService.log(LogService.LOG_INFO, logMsg);
    }

    void info(String msg, Object... args) {
        String logMsg = String.format(msg, args);
        m_logService.log(LogService.LOG_INFO, logMsg);
    }

    void warn(String msg, Object... args) {
        warn(msg, null, args);
    }

    void warn(String msg, Exception failure, Object... args) {
        String logMsg = String.format(msg, args);
        m_logService.log(LogService.LOG_WARNING, logMsg, failure);
    }

    void error(String msg, Object... args) {
        String logMsg = String.format(msg, args);
        m_logService.log(LogService.LOG_WARNING, logMsg);
    }

    protected void provision(PropertyFileResource resource) throws IOException {
        Dictionary<String, ?> props = resource.getConfiguration();

        String pid = resource.getPid();
        String factoryPID = resource.getFactoryPid();

        Configuration configuration;
        if (resource.isFactoryConfig()) {
            String generatedPid = m_repository.getConfigurationPid(resource);
            if (generatedPid == null) {
                if (isVerbose()) {
                    info("Creating new configuration for \"%s\" (not found in repository)...", factoryPID);
                }
                // See OSGi compendium r4.2.0, section 114.4.1...
                configuration = m_configAdmin.createFactoryConfiguration(factoryPID, null);
            }
            else {
                if (isVerbose()) {
                    info("Obtaining configuration for \"%s\" (found in repository)...", generatedPid);
                }
                // See OSGi compendium r4.2.0, section 114.4.1...
                configuration = m_configAdmin.getConfiguration(generatedPid, null);
            }
        }
        else {
            if (isVerbose()) {
                info("Obtaining configuration for \"%s\" (maybe found in repository)...", pid);
            }
            // See OSGi compendium r4.2.0, section 114.4.1...
            configuration = m_configAdmin.getConfiguration(pid, null);
        }

        if (configuration != null) {
            if (m_repository.add(configuration, resource)) {
                info("Provisioned new Property-file resource: %s", resource.getId());
            }
            configuration.update(props);
        }
    }

    /**
     * Called by Felix DM when starting this component.
     */
    protected void start(Component component) throws IOException {
        File repositoryFile = getRepositoryFile();
        // Load the earlier provisioned configurations...
        m_repository.load(repositoryFile);

        try {
            install();
        }
        finally {
            // Store the earlier provisioned configurations...
            m_repository.store(repositoryFile);
        }
    }

    /**
     * Called by Felix DM when stopping this component.
     */
    protected void stop(Component component) throws IOException {
        File repositoryFile = getRepositoryFile();
        // Store the earlier provisioned configurations (just to be sure)...
        m_repository.store(repositoryFile);
    }

    private File getConfigDirectory() {
        String dir = m_context.getProperty(KEY_CONFIG_DIR);
        if (dir == null) {
            dir = DEFAULT_CONFIG_DIR;
        }
        return new File(dir);
    }

    private String getPlaceholderPrefix() {
        String prefix = m_context.getProperty(KEY_PLACEHOLDER_PREFIX);
        if (prefix == null) {
            prefix = DEFAULT_PLACEHOLDER_PREFIX;
        }
        return prefix;
    }

    private File getRepositoryFile() throws IOException {
        File dataArea = m_context.getDataFile("");
        if (dataArea == null) {
            throw new IOException("No peristent storage supported...");
        }
        return new File(dataArea, "properties.repo");
    }

    private boolean isVerbose() {
        return Boolean.parseBoolean(m_context.getProperty(KEY_VERBOSE));
    }

    private File[] listConfigurations() {
        File dir = getConfigDirectory();
        if (!dir.exists()) {
            warn("Property-file configuration directory \"%s\" does not exist!", dir);
            return new File[0];
        }
        return dir.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".cnf") || name.endsWith(".cfg") || name.endsWith(".config") || name.endsWith(".props") || name.endsWith(".properties");
            }
        });
    }

    class FrameworkPropertyReplacer implements Replacer {
        @Override
        public String replace(String input) {
            if (input == null) {
                return null;
            }
            String prefix = getPlaceholderPrefix();
            if (input.startsWith(prefix)) {
                input = input.substring(prefix.length());
            }
            return m_context.getProperty(input);
        }
    }

    class LogBridge implements Logger {
        @Override
        public void debug(String msg, Object... args) {
            debug(msg, args);
        }

        @Override
        public void error(String msg, Object... args) {
            error(msg, args);
        }

        @Override
        public void info(String msg, Object... args) {
            info(msg, args);
        }

        @Override
        public void warn(String msg, Object... args) {
            warn(msg, args);
        }
    }
}
