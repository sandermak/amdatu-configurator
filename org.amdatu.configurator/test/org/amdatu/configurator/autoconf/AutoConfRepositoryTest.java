/*
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.configurator.autoconf;

import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.WRITE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;

import org.apache.felix.metatype.Designate;
import org.junit.Test;
import org.osgi.service.cm.Configuration;

/**
 * Test cases for {@link AutoConfRepository}.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class AutoConfRepositoryTest {

    @Test
    public void testAddOk() throws IOException {
        Configuration config1 = createConfiguration("myPid");
        AutoConfResource resource1 = createResource("myPid");
        AutoConfResource resource2 = createResource("factory", "myPid");

        AutoConfRepository repo = new AutoConfRepository();
        assertTrue(repo.add(config1, resource1));
        assertFalse(repo.add(config1, resource1));

        assertEquals("myPid", repo.getConfigurationPid(resource1));
        assertNull(repo.getConfigurationPid(resource2));
    }

    @Test
    public void testLoadEmptyRepositoryOk() throws IOException {
        File repoFile = createRepositoryFile();

        AutoConfRepository repo = new AutoConfRepository();
        repo.load(repoFile); // should succeed.
        assertTrue(repo.isEmpty());
    }

    @Test(expected = IOException.class)
    public void testLoadInvalidRepositoryOk() throws IOException {
        File repoFile = createRepositoryFile();
        Files.write(repoFile.toPath(), "invalid data".getBytes());

        AutoConfRepository repo = new AutoConfRepository();
        repo.load(repoFile); // should fail...
    }

    @Test
    public void testLoadNonExistingRepositoryOk() throws IOException {
        File repoFile = createRepositoryFile();
        repoFile.delete();

        AutoConfRepository repo = new AutoConfRepository();
        repo.load(repoFile); // should succeed.
        assertTrue(repo.isEmpty());
    }

    @Test
    public void testLoadRepositoryOk() throws IOException {
        File repoFile = createRepositoryFile();
        writeBogusRepositoryData(repoFile);

        AutoConfRepository repo = new AutoConfRepository();
        repo.load(repoFile); // should succeed.
        assertFalse(repo.isEmpty());
    }

    @Test
    public void testStoreEmptyRepositoryOk() throws IOException {
        File repoFile = createRepositoryFile();

        AutoConfRepository repo = new AutoConfRepository();
        repo.store(repoFile); // should succeed.

        repo.load(repoFile); // should succeed.
        assertTrue(repo.isEmpty());
    }

    @Test
    public void testStoreRepositoryOk() throws IOException {
        File repoFile = createRepositoryFile();

        AutoConfResource resource1 = createResource("myPid");
        AutoConfResource resource2 = createResource("factory", "myPid");

        AutoConfRepository repo = new AutoConfRepository();
        repo.add(createConfiguration("myPid"), resource1);

        repo.store(repoFile); // should succeed.

        repo.load(repoFile); // should succeed.
        assertFalse(repo.isEmpty());
        assertEquals("myPid", repo.getConfigurationPid(resource1));
        assertNull(repo.getConfigurationPid(resource2));
    }

    private Configuration createConfiguration(String pid) {
        return createConfiguration(null, pid);
    }

    private Configuration createConfiguration(String factoryPID, String pid) {
        Configuration config = mock(Configuration.class);
        when(config.getFactoryPid()).thenReturn(factoryPID);
        when(config.getPid()).thenReturn(pid);
        return config;
    }

    private File createRepositoryFile() throws IOException {
        File repoFile = File.createTempFile("confrepo", ".ser");
        repoFile.deleteOnExit();
        return repoFile;
    }

    private AutoConfResource createResource(String pid) {
        return createResource(null, pid);
    }

    private AutoConfResource createResource(String factoryPID, String pid) {
        Designate designate = mock(Designate.class);
        when(designate.getFactoryPid()).thenReturn(factoryPID);
        when(designate.getPid()).thenReturn(pid);

        AutoConfResource res = mock(AutoConfResource.class);
        when(res.getDesignate()).thenReturn(designate);
        return res;
    }

    private void writeBogusRepositoryData(File repoFile) throws IOException {
        Map<String, String> data = new HashMap<>();
        data.put("key1", "value1");
        data.put("key2", "value2");
        try (OutputStream fos = Files.newOutputStream(repoFile.toPath(), WRITE, APPEND); ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            oos.writeObject(data);
        }
    }
}
