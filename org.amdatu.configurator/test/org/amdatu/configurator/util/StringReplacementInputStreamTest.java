/*
 * Copyright (c) 2010-2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.configurator.util;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Scanner;

import org.amdatu.configurator.util.StringReplacementInputStream;
import org.amdatu.configurator.util.StringReplacementInputStream.Replacer;
import org.junit.Test;

/**
 * Test cases for {@link StringReplacementInputStream}.
 * 
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class StringReplacementInputStreamTest {

    @Test
    public void testHandleEmptyStreamOk() throws Exception {
        InputStream is = createInputStream("", createUppercaseReplacer());
        assertReplacement(is, "");
    }

    @Test
    public void testHandleStreamPartialPlaceholderOk() throws Exception {
        InputStream is = createInputStream("${one", createNumericReplacer());
        assertReplacement(is, "${one");
    }

    @Test
    public void testHandleStreamPartialReplacementOk() throws Exception {
        InputStream is = createInputStream("testing ${zero} ${one} ${two} ${three} ${four}!", createNumericReplacer());
        assertReplacement(is, "testing ${zero} 1 2 3 ${four}!");
    }

    @Test
    public void testHandleStreamWithEmbeddedPlaceholderOk() throws Exception {
        InputStream is = createInputStream("hello ${${name}}", createUppercaseReplacer());
        assertReplacement(is, "hello ${NAME}");
    }

    @Test
    public void testHandleStreamWithoutPlaceholderOk() throws Exception {
        InputStream is = createInputStream("hello world", createUppercaseReplacer());
        assertReplacement(is, "hello world");
    }

    @Test
    public void testHandleStreamWithoutReplacementOk() throws Exception {
        InputStream is = createInputStream("hello ${first} ${last}!", createNullReplacer());
        assertReplacement(is, "hello ${first} ${last}!");
    }

    @Test
    public void testHandleStreamWithSinglePlaceholderOk() throws Exception {
        InputStream is = createInputStream("hello ${name}", createUppercaseReplacer());
        assertReplacement(is, "hello NAME");
    }

    @Test
    public void testHandleStreamWithTwoPlaceholderOk() throws Exception {
        InputStream is = createInputStream("hello ${first} ${last}!", createUppercaseReplacer());
        assertReplacement(is, "hello FIRST LAST!");
    }

    private void assertReplacement(InputStream is, String expected) {
        // See <weblogs.java.net/blog/pat/archive/2004/10/stupid_scanner_1.html>
        try (Scanner scanner = new Scanner(is, "UTF-8")) {
            scanner.useDelimiter("\\A");

            assertEquals(expected, scanner.hasNext() ? scanner.next() : "");
        }
    }

    private InputStream createInputStream(String string, Replacer replacer) {
        return new StringReplacementInputStream(new ByteArrayInputStream((string == null ? "" : string).getBytes()), replacer);
    }

    private Replacer createNullReplacer() {
        return new Replacer() {
            @Override
            public String replace(String input) {
                return null;
            }
        };
    }

    private Replacer createNumericReplacer() {
        return new Replacer() {
            @Override
            public String replace(String input) {
                if ("one".equals(input)) {
                    return "1";
                }
                else if ("two".equals(input)) {
                    return "2";
                }
                else if ("three".equals(input)) {
                    return "3";
                }
                return null;
            }
        };
    }

    private Replacer createUppercaseReplacer() {
        return new Replacer() {
            @Override
            public String replace(String input) {
                if (input.matches("^[A-Z]$")) {
                    return input.toLowerCase();
                }
                return input.toUpperCase();
            }
        };
    }
}
