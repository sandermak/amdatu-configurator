# Amdatu Configurator

This project provides a simple way of provisioning metatype/autoconf based configuration files to your OSGi application,
allowing you to use the same set of configuration files to run your OSGi application locally as you would need for
deploying your application using Apache ACE or DeploymentAdmin. 

It provides the following features:

- provisions MetaType/AutoConf based configuration files;
- allow simple variable substitutions similar as to Apache ACE;
- will provide simple Felix Gogo shell integration.

What this project does *not* provide:

- a FileInstall like functionality, where configuration files and bundles are automatically monitored and updated if necessary.

## Usage

Deploy the `org.amdatu.configurator.autoconf` bundle with your application. When this bundle is started by the OSGi
framework, it will look for a directory named `conf` in the current working directory of your framework and regard all
files ending in `.xml` as possible AutoConf resources. Messages and errors are reported to the OSGi LogService (if
available).

### Dependencies

The Amdatu Configurator bundle depends on the following bundles:

1. Apache Felix DependencyManager v3.1.0;
2. An OSGi ConfigurationAdmin service implementation, for example Apache Felix ConfigAdmin v1.8.0;
3. (Optionally) an OSGi LogService implementation, for example Apache Felix LogService v1.0.1.


### Configuration

A couple of configuration options exist for the Amdatu Configurator project. These options can be set as framework
option in the run configuration of your application. The following options are recognized:

- `org.amdatu.configurator.autoconf.dir`, takes a string argument that is used as directory where your configuration
  files are stored. Defaults to `./conf`;
- `org.amdatu.configurator.autoconf.prefix`, allows one to specify the common prefix used for all placeholders in your
  configuration files. By default, the same prefix is used as Apache ACE, which is `context.`;
- `org.amdatu.configurator.autoconf.verbose`, increases the verbosity of logging to show more details on the whole
  provisioning process. By default, non-verbose/sparse logging is enabled.

### Variable/placeholder substitution

Though not allowed by the AutoConf specification, the Amdatu Configurator allows AutoConf resources to be parameterized
with variables (or placeholders) in order to make them a little more reusable for different deployment scenarios.
Variables are considered all strings that start with <tt>${</tt> and end with <tt>}</tt>. When an AutoConf resource is
processed by Amdatu Configurator, it will try to replace all variables with values found as framework properties. So,
for example, a variable `${context.serverUrl}` will be replaced with the value of the framework property `serverUrl`
(the `context.` prefix is stripped off, see [configuration](#configuration)). If no framework property is found, no
substitution will take place.

### Example

Consider the following AutoConf resource, named `myConfig.xml`, stored in a directory named `config`:

    <?xml version="1.0" encoding="UTF-8"?>
    <metatype:MetaData xmlns:metatype="http://www.osgi.org/xmlns/metatype/v1.1.0">
        <OCD id="ocd" name="ocd">
            <AD id="server" type="String" />
        </OCD>
    
        <Designate pid="org.amdatu.conf" bundle="org.amdatu.sample.bundle">
            <Object ocdref="ocd">
                <Attribute adref="server" name="serverurl" content="${context.serverUrl}" />
            </Object>
        </Designate>
    </metatype:MetaData>

If we would start our OSGi application, for example, using a Bnd Run Descriptor with the following content:

    -runbundles: \
       osgi.cmpn;version="[4.2,5)",\
       org.apache.felix.configadmin;version="[1.8,1.9)",\
       org.apache.felix.dependencymanager;version="[3.1,3.2)",\
       org.apache.felix.log;version="[1.0.1,1.0.2)",\
       org.amdatu.configurator.autoconf;version=latest
    # ...many more properties...
    -runproperties: \
       serverUrl=http://my.server.host:8080/,\
       org.amdatu.configurator.autoconf.dir=/path/to/config

The Amdatu Configurator will provision a configuration with a single property of `serverUrl =
http://my.server.host:8080/` to ConfigAdmin. The result will be that this property will be given to the ManagedService
implementation registered with the `org.amdatu.conf` PID.

## Links

* [Amdatu Website](http://www.amdatu.org/components/configurator.html);
* [Source Code](https://bitbucket.org/amdatu/amdatu-configurator);
* [Issue Tracking](https://amdatu.atlassian.net/browse/AMDATUCONF);
* [Development Wiki](https://amdatu.atlassian.net/wiki/display/AMDATUDEV/Amdatu+Configurator);
* [Continuous Build](https://amdatu.atlassian.net/builds/browse/AMDATUCONF).

## License

The Amdatu Configurator project is licensed under [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0.html).

